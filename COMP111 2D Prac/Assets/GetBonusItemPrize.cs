﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;


public class GetBonusItemPrize : MonoBehaviour {
	private HingeJoint _hingejoint;
	private bool isGone = false;
	public GameObject unlockGoal;
	// Use this for initialization
	
	void Start () {

	}
	
	

	// Update is called once per frame
	
	void Update () {
	
	}


	public void OnTriggerEnter2D(Collider2D collider) {
		if (isGone) return;
		isGone = true;
		unlockGoal = GameObject.Find("BonusItemPlatformChain2");
		_hingejoint = unlockGoal.GetComponent<HingeJoint>();
		JointMotor motor = _hingejoint.motor;
		motor.force = 0;
	}
}
